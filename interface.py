#!/usr/bin/env python

import os
import subprocess
import torndb
import tornado.escape
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import subprocess
from tornado.options import define, options

try:
   define("port", group="root", default=8888, help="run on the given port", type=int)
   define("listen_address", group="root", default="127.0.0.1", help="run on the given listen address")
except Exception:
   sys.exit()

class Application(tornado.web.Application):
    def __init__(self):
           handlers = [
               (r"/", HomeHandler),
               (r".*", HomeHandler),
           ]

           settings = dict(
               blog_title=u"Management Router",
               template_path=os.path.join(os.path.dirname(__file__), "templates"),
               static_path=os.path.join(os.path.dirname(__file__), "static"),
               debug=True,
           )
           super(Application, self).__init__(handlers, **settings)

           if not handlers:
            self.redirect("/",page=None)


class HomeHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("home.html")

    def post(self):
        username = self.get_argument("username")
        password = self.get_argument("password")
        hostname = self.get_argument("hostname")
        print(username+password+hostname)

        if hostname != "" or username != "" or password != "":
           try:
              pb = subprocess.Popen(["sshpass", "-p", password, "ssh","-o", "ConnectTimeout=3", "-l"+username,hostname, "/system", "backup", "save", "name="+hostname])
              stsb = os.waitpid(pb.pid, 0)
              p = subprocess.Popen(["sshpass", "-p", password, "scp", username+"@"+hostname+":/{name}.backup", hostname+".backup"])
              sts = os.waitpid(p.pid, 0)
              status = "Berhasil backup, data tersimpan di /home."
              self.write("<script>alert('"+status+"'); location.replace(document.referrer);</script>")

           except Exception:
              status = "Backup Failed !!"
              self.write("<script>alert('"+status+"'); location.replace(document.referrer);</script>")
        else:
           self.write("<script>alert('Kolom Tidak Boleh Kosong !!'); location.replace(document.referrer);</script>")
           pass

def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port,address=options.listen_address)
    tornado.ioloop.IOLoop.current().start()

if __name__ == "__main__":
    main()
